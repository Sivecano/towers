const data = @import("entities.zig");
const TowerType = data.TowerType;
const projectiles = @import("projectiles.zig");


pub var basic: TowerType = .{
    .firerate = 4,
    .projectile = projectiles.basic_projectile,
    .texture = .{
            .filepath = "resources/basic_tower.png",
    },
};
