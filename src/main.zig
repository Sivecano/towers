const std = @import("std");
const lib = @import("entities.zig");
const map = @import("map.zig");
const sdl = @import("sdl2");

const towers = @import("towers.zig");

const playfield: lib.Vec2 = .{.x = 40, .y = 40};
const scale = 30;

const myMap: map.Map = .{
        .path = .{ .waypoints = &[_]lib.Vec2{
                .{.x = 0, .y = 0 },
                .{.x = 10, .y = 0 },
                .{.x = 10, .y = 20 },
                .{.x = 40, .y = 40 },
            }
        },
        .background = .{ .filepath = "back.png" },
        .size = .{.x = 80, .y = 80},
        .scale  = 50,
    };

pub fn main() !void
{
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = arena.allocator();

    try sdl.init(.{ .events = true, .video = true, .timer = true});
    defer sdl.quit();

    var win = try sdl.createWindow("tower defense",
        .{.centered = {}}, .{.centered = {}},
        playfield.x * scale, playfield.y * scale, .{});
    defer win.destroy();

    var ren = try sdl.createRenderer(win, 0, .{.accelerated = true});
    defer ren.destroy();

    var state = map.GameState.init(.{.map = myMap, .wave = .{}}, alloc);
    defer state.deinit();

    try state.enemies.append(.{.health = 20, .speed = 0.002, .pos = .{.x = 0, .y = 0}, .pathpoint = 0});

    try state.towers.append(try towers.basic.instantiate(.{.x = 4, .y = 4}, ren));

    // try path.addWaypoint(.{.x = 800, .y = 800});

    var lastframe =std.time.microTimestamp();
    gameloop: while (true)
    {
        const nowframe = std.time.microTimestamp();
        const frametime: f32 =  @as(f32, @floatFromInt((nowframe - lastframe))) / 1000;
        defer lastframe = nowframe;


        //// handle events
        while (sdl.pollEvent()) |event |
        {
            switch(event){
                .quit => break :gameloop,
                else => {},
            }
        }


        //// update state

        { ///// projectiles
            var removalqueue = std.ArrayList(usize).init(alloc);
            defer removalqueue.deinit();

            for (state.projectiles.items, 0..) |*p, pi|
            {
                if (p.pos.x < 0 or p.pos.y < 0 or p.pos.x > playfield.x or p.pos.y > playfield.y) {
                    try removalqueue.append(pi);
                    continue;
                }

                var eremovalqueue = std.ArrayList(usize).init(alloc);
                defer eremovalqueue.deinit();

                p.pos.x += p.speed.x * frametime;
                p.pos.y += p.speed.y * frametime;

                for (state.enemies.items, 0..) |*e, ei|
                {
                    if (p.touches(e.*))
                    {
                        e.health -= p.type.damage;
                        try removalqueue.append(pi);

                        if (e.health <= 0)
                        {
                            try eremovalqueue.append(ei);
                        }
                    }
                }

                while (eremovalqueue.popOrNull()) |ei|{
                    _ = state.enemies.swapRemove(ei);
                }

            }

            while (removalqueue.popOrNull()) |pi| {
                _ = state.projectiles.swapRemove(pi);
            }
        }

         // towers
        for (state.towers.items) |*t|
        {
            if (state.enemies.items.len == 0)
                continue;

           var min = state.enemies.getLast().pos;
           const pos = t.pos;
           for (state.enemies.items) |e|
           {
                if (pos.distance(e.pos) < pos.distance(min))
                    min = e.pos;
           }

          // TODO: check firerate
          t.cooldown -= frametime;
          if (t.cooldown <= 0) {
              t.cooldown = 1000 / t.type.firerate;
              try state.projectiles.append(try t.type.projectile.instantiate(t.pos, min.diff(pos), ren));
          }
        }


        // enemies
        for (state.enemies.items) |*e| {
            e.pathpoint += e.speed * frametime;
            e.pos = state.game.map.path.travelpoint(e.pathpoint);
        }


        /////// rendering

        try ren.setColorRGB(120, 120, 120);
        try ren.clear();

        try state.renderEnemies(ren);

        try state.renderProjectiles(ren);

        // _ = sdl.c.SDL_RenderDrawLinesF(ren.ptr, @ptrCast(path.waypoints.items), @intCast(path.waypoints.items.len));

        try state.renderTowers(ren);

        for (1..state.game.map.path.waypoints.len) |i| {
            const wp = state.game.map.path.waypoints;
            try ren.drawLineF(wp[i-1].x * state.game.map.scale, wp[i-1].y * state.game.map.scale, wp[i].x * state.game.map.scale, wp[i].y * state.game.map.scale);
        }

        ren.present();

        std.debug.print("\rframetime: {d:5.2}, fps: {d:6.2}", .{frametime, 1000 / frametime});
    }

}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
