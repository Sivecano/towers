const std = @import("std");
const math = std.math;
const data = @import("map.zig");
const sdl = @import("sdl2");

const TrackedImage = data.TrackedImage;
const Image = data.Image;

pub const Vec2 = struct {
    x: f32,
    y: f32,

    pub fn length(self: Vec2) f32 {
        return math.sqrt(self.x * self.x + self.y * self.y);
    }
    
    pub fn distance(self: Vec2, other: Vec2) f32 {
        return self.diff(other).length();
    }

    pub fn add(self: Vec2, other: Vec2) Vec2 {
        return .{ .x = self.x + other.x, .y = self.y + other.y};
    }

    pub fn diff(self: Vec2, other: Vec2) Vec2 {
        return .{ .x = self.x - other.x, .y = self.y - other.y};
    }

    pub fn times(self: Vec2, factor: f32) Vec2 {
        return .{.x = self.x * factor, .y = self.y * factor};
    }
};

pub const Enemy = struct {
    health : f32,
    speed : f32,
    pos: Vec2,
    pathpoint: f32 = 0,
};


pub const TowerType = struct {
    firerate: f32,
    projectile: ProjectileType,
    texture: Image,

    pub fn instantiate(self: TowerType, pos: Vec2, ren: sdl.Renderer) !Tower {
        return .{.pos = pos, .type = self, .texture = try self.texture.load(ren) };
    }
};


pub const Tower = struct {
    pos: Vec2,
    cooldown: f32 = 0,

    type: TowerType,
    texture: sdl.Texture,
};

pub const ProjectileType = struct {
    radius: f32,
    damage: f32,
    speed: f32,

    texture: Image,

    pub fn instantiate(self: ProjectileType, pos: Vec2, direction: Vec2 ,ren: sdl.Renderer) !Projectile {
        return .{
            .pos = pos,
            .speed = direction.times(self.speed / direction.length()),
            .texture = try self.texture.load(ren),
            .type = self
        };
    }
};

pub const Projectile = struct {
    pos: Vec2,
    speed: Vec2,

    texture: sdl.Texture,
    type: ProjectileType,

    pub fn touches(self: Projectile, other: Enemy) bool {
        const radius = self.type.radius;
        return math.pow(f32, self.pos.x - other.pos.x, 2) +
                math.pow(f32, self.pos.y - other.pos.y, 2)
                    < radius + 0.5;
    }
};

pub const Path = struct {
    waypoints: []const Vec2,

    pub fn travelpoint(self: Path, distance: f32) Vec2 {
        const wp = self.waypoints;

        if (wp.len < 1 or distance <= 0)
            return wp[0];

        var dist = distance;
        for (wp[0..wp.len - 1], wp[1..]) |f, s| {
            const d = f.distance(s);

            if (d < dist){
                dist -= d;
            } else {
                const dvec = s.diff(f);
                const l = dist / d;
                return f.add(dvec.times(l));
            }
        }

        return wp[wp.len - 1];
    }

    pub fn length(self: Path) f32
    {
        const wp = self.waypoints;
        var acc: f32 = 0;

        for (wp.items[0..wp.len - 1], wp[1..]) |f, s|
            acc += f.distance(s);

        return acc;
    }
};


test "travelpoint"{
    var path = Path.init(std.testing.allocator);
    defer path.deinit();

    const eq = std.testing.expectEqual;
    try path.addWaypoint(.{.x = 0, .y = 0});
    try eq(0, path.length());
    try path.addWaypoint(.{.x = 10, .y = 0});
    try eq(10, path.length());
    try path.addWaypoint(.{.x = 10, .y = 20});
    try path.addWaypoint(.{.x = 40, .y = 40});

    const wp = path.waypoints.items;

    try eq(wp[0], path.travelpoint(0));
    
}
