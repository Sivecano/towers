const data = @import("entities.zig");
const ProjectileType = data.ProjectileType;

pub const basic_projectile: ProjectileType = .{
    .radius = 0.2,
    .speed = 0.015,
    .damage = 1,

    .texture = .{
            .filepath = "resources/basic_tower_projectile.png",
    },
};

