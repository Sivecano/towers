const std = @import("std");
const entities = @import("entities.zig");
const sdl = @import("sdl2");

const Storage = enum {
    filepath,
    embedded,
};

pub const Image = union(Storage) {
    filepath: [:0]const u8,
    embedded: [:0]u8,

    pub fn load(self: Image, ren: sdl.Renderer) !sdl.Texture {
        return switch(self) {
            .filepath => |path| try sdl.image.loadTexture(ren, path),
            .embedded => |data| try sdl.image.loadTextureMem(ren, data, .png),
        };
    }

};

pub const TrackedImage = struct {
    refs: u8 = 0,
    file : Image,
    tex: ?sdl.Texture = null,

    pub fn load(self: *TrackedImage, ren: sdl.Renderer) !sdl.Texture {
        self.refs += 1;
        if (self.tex == null)
            self.tex = self.file.load(ren);

        return self.tex;
    }

    pub fn unload(self: *TrackedImage) void {

        if (self.refs > 0)
            self.refs -= 1;

        if (self.refs == 0 and self.tex != null)
            self.tex.?.destroy();

        self.tex = null;
    }

};



pub const Map = struct {
    path: entities.Path,
    background: Image,

    size: entities.Vec2,
    scale: f32,
};

pub const Wave = struct {

};

pub const Game = struct {
    map: Map,
    wave: Wave,
};

pub const GameState = struct {
    game: Game,

    enemies: std.ArrayList(entities.Enemy),
    towers: std.ArrayList(entities.Tower),
    projectiles: std.ArrayList(entities.Projectile),

    pub fn init(game: Game, alloc: std.mem.Allocator) GameState {
        return .{   .game = game,
                    .enemies = std.ArrayList(entities.Enemy).init(alloc),
                    .towers = std.ArrayList(entities.Tower).init(alloc),
                    .projectiles = std.ArrayList(entities.Projectile).init(alloc),
                    
        };
    }

    pub fn deinit(self: GameState) void {
        self.enemies.deinit();
        self.towers.deinit();
        self.projectiles.deinit();
    }

    pub fn renderTowers(self: GameState, ren: sdl.Renderer) !void {
        const scale = self.game.map.scale;

        try ren.setColorRGB(20, 100, 100);

        for (self.towers.items) |t|
            try ren.copyF(t.texture, .{ .x = t.pos.x * scale, .y = t.pos.y * scale, .width = scale, .height = scale}, null);
    }

    pub fn renderEnemies(self: GameState, ren: sdl.Renderer) !void {
        const scale = self.game.map.scale;

        try ren.setColorRGB(200, 20, 20);

        for (self.enemies.items) |p|
            try ren.fillRectF( .{ .x = p.pos.x * scale, .y = p.pos.y * scale, .width = scale, .height = scale });

    }

    pub fn renderProjectiles(self: GameState, ren: sdl.Renderer) !void {
        const scale = self.game.map.scale;

        try ren.setColorRGB(20, 20, 180);

        for (self.projectiles.items) |p| {
            var angle = std.math.deg_per_rad * std.math.atan(p.speed.y/p.speed.x) + 90;
            if (p.speed.x < 0)
                angle += 180;

            try ren.copyExF(p.texture,
                        .{ .x = p.pos.x * scale, .y = p.pos.y * scale, .width = scale, .height = scale },
                        null,
                         angle,
                        null,
                        .none );
        }
            // try ren.fillRectF( .{ .x = p.pos.x * scale, .y = p.pos.y * scale, .width = scale, .height = scale });
    }


};
